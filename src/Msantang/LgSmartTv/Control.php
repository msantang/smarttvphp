<?php

namespace Msantang\LgSmartTv;

/**
 * Provide a cleaner interface to control the SmartTv
 *
 * @author Martin Alejandro Santangelo <msantangelo@smartsoftware.com.ar>
 */
class Control
{
    protected static $codes = [
        'CMD_POWER'                    => 1,
        'CMD_0'                        => 2,
        'CMD_1'                        => 3,
        'CMD_2'                        => 4,
        'CMD_3'                        => 5,
        'CMD_4'                        => 6,
        'CMD_5'                        => 7,
        'CMD_6'                        => 8,
        'CMD_7'                        => 9,
        'CMD_8'                        => 10,
        'CMD_9'                        => 11,
        'CMD_UP'                       => 12,
        'CMD_DOWN'                     => 13,
        'CMD_LEFT'                     => 14,
        'CMD_RIGHT'                    => 15,
        'CMD_OK'                       => 20,
        'CMD_HOME_MENU'                => 21,
        'CMD_BACK'                     => 23,
        'CMD_VOLUME_UP'                => 24,
        'CMD_VOLUME_DOWN'              => 25,
        'CMD_MUTE_TOGGLE'              => 26,
        'CMD_CHANNEL_UP'               => 27,
        'CMD_CHANNEL_DOWN'             => 28,
        'CMD_BLUE'                     => 29,
        'CMD_GREEN'                    => 30,
        'CMD_RED'                      => 31,
        'CMD_YELLOW'                   => 32,
        'CMD_PLAY'                     => 33,
        'CMD_PAUSE'                    => 34,
        'CMD_STOP'                     => 35,
        'CMD_FAST_FORWARD'             => 36,
        'CMD_REWIND'                   => 37,
        'CMD_SKIP_FORWARD'             => 38,
        'CMD_SKIP_BACKWARD'            => 39,
        'CMD_RECORD'                   => 40,
        'CMD_RECORDING_LIST'           => 41,
        'CMD_REPEAT'                   => 42,
        'CMD_LIVE_TV'                  => 43,
        'CMD_EPG'                      => 44,
        'CMD_PROGRAM_INFORMATION'      => 45,
        'CMD_ASPECT_RATIO'             => 46,
        'CMD_EXTERNAL_INPUT'           => 47,
        'CMD_PIP_SECONDARY_VIDEO'      => 48,
        'CMD_SHOW_SUBTITLE'            => 49,
        'CMD_PROGRAM_LIST'             => 50,
        'CMD_TELE_TEXT'                => 51,
        'CMD_MARK'                     => 52,
        'CMD_3D_VIDEO'                 => 400,
        'CMD_3D_LR'                    => 401,
        'CMD_DASH'                     => 402,
        'CMD_PREVIOUS_CHANNEL'         => 403,
        'CMD_FAVORITE_CHANNEL'         => 404,
        'CMD_QUICK_MENU'               => 405,
        'CMD_TEXT_OPTION'              => 406,
        'CMD_AUDIO_DESCRIPTION'        => 407,
        'CMD_ENERGY_SAVING'            => 409,
        'CMD_AV_MODE'                  => 410,
        'CMD_SIMPLINK'                 => 411,
        'CMD_EXIT'                     => 412,
        'CMD_RESERVATION_PROGRAM_LIST' => 413,
        'CMD_PIP_CHANNEL_UP'           => 414,
        'CMD_PIP_CHANNEL_DOWN'         => 415,
        'CMD_SWITCH_VIDEO'             => 416,
        'CMD_APPS'                     => 417,
        'CMD_MOUSE_MOVE'               => 'HandleTouchMove',
        'CMD_MOUSE_CLICK'              => 'HandleTouchClick',
        'CMD_TOUCH_WHEEL'              => 'HandleTouchWheel',
        'CMD_CHANGE_CHANNEL'           => 'HandleChannelChange',
        'CMD_SCROLL_UP'                => 'up',
        'CMD_SCROLL_DOWN'              => 'down',
        'CMD_LAUNCH_APP'               => 'AppExecute',

        'GET_CURRENT_CHANNEL'         => 'cur_channel',
        'GET_CHANNEL_LIST'            => 'channel_list',
        'GET_CONTEXT_UI'              => 'context_ui',
        'GET_VOLUME'                  => 'volume_info',
        'GET_SCREEN'                  => 'screen_image',
        'GET_APPS'                    => 'applist_get',
        'GET_3D'                      => 'is_3d'
    ];
    /**
     * @var SmartTV
     */
    protected $smartTv;

    public function __construct(SmartTV $stv)
    {
        $this->smartTv = $stv;
    }

    public function getSmartTv()
    {
        return $this->smartTv;
    }

    public function __call($name, $arguments)
    {
        if (@$arguments[0]) {
            $args = @$arguments[0];
        } else {
            $args = [];
        }

        if (@$arguments[1]) {
            $fn = $arguments[1];
        } else {
            $fn = null;
        }

        if (substr($name,0,3) == 'cmd') {
            $c = strtoupper(preg_replace('/([a-z])([A-Z0-9])/', '$1_$2', $name));

            return $this->smartTv->processCommand(self::$codes[$c], $args, $fn);
        }

        if (substr($name,0,3) == 'get') {
            $c = strtoupper(preg_replace('/([a-z])([A-Z0-9])/', '$1_$2', $name));

            return $this->smartTv->queryData(self::$codes[$c], $args, $fn);
        }
    }

}