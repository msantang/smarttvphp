<?php
namespace Msantang\LgSmartTv;

use SimpleXMLElement;
/**
 * @author Martin Alejandro Santangelo <msantangelo@smartsoftware.com.ar>
 */
class Command
{
    private $type;

    private $params;

    public function __construct($params=[], $type='command')
    {
        $this->params = $params;
        $this->type   = $type;
    }

    static protected function encode($value, $type, $xml=null)
    {
        if ($xml == null) {
            $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\"?><envelope><api type=\"$type\"></api></envelope>");
            $x = $xml->api;
        } else {
            $x = $xml;
        }
        foreach($value as $key => $value) {

            $x->addChild($key, htmlentities($value));

        }
        return $xml->asXML();
    }

    public function __toString()
    {
        return $this->encode($this->params, $this->type);
    }
}
