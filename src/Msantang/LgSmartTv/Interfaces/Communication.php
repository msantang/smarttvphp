<?php

namespace Msantang\LgSmartTv\Interfaces;

use Msantang\LgSmartTv\Command;

interface Communication {
    public function send($url, Command $data=null);
}