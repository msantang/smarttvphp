<?php

namespace Msantang\LgSmartTv;

use Msantang\LgSmartTv\Interfaces\Communication;
use Msantang\LgSmartTv\Communication\UnpairedException;

/**
 * @author Martin Alejandro Santangelo <msantangelo@smartsoftware.com.ar>
 */

/**
 * Some constants
 */

/*
print_r( $tv->processCommand(TV_LAUNCH_APP,['auid' => '00000000000112ae', 'appname' => 'Netflix']) );
*/

class SmartTV {

    public $debug = true;
    private $pairingKey;
    public $session;
    private $eventPort;

    protected $comm;

    public function __construct(Communication $comm, $eventPort=8080) {
        $this->comm      = $comm;
        $this->eventPort = $eventPort;
    }

    public function setPairingKey($pk) {
        $this->pairingKey = $pk;
    }

    public function showKey() {
        $this->comm->send('/udap/api/pairing', new Command(
            array('name' => 'showKey'), 'pairing'
        ));
    }

    public function setSession($sess) {
        $this->session = $sess;
    }

    public function authenticate() {
        if ($this->pairingKey === null) {
            throw new Exception('No pairing key given.');
        }

        $p = array(
            'name'  => 'hello',
            'value' => $this->pairingKey
        );

        if ($this->eventPort) $p['port'] = $this->eventPort;

        $that = $this;

        $p = $this->comm->send('/udap/api/pairing', new Command($p, 'pairing'), function()use($that){
            $that->session = 1;
        });

        $p->wait();
    }

    protected function send($url, $data=null ,$fn=null) {
        try {
            return $this->comm->send($url, $data, $fn);
        }
        catch (UnpairedException $e) {
            $this->authenticate();
            return $this->comm->send($url, $data, $fn);
        }
    }

    public function bye()
    {
        return $this->send('/udap/api/pairing', new Command(
            array(
                'name'  => 'byebye',
                'value' => $this->pairingKey
            ),
            'pairing'
        ));
    }

    public function appMessage($auid, $message)
    {

        return $this->comm->sendRaw('/udap/api/apptoapp/command/'.strtoupper(sprintf("%08x",$auid)).'/send', $message);
    }

    public function processCommand($commandName, $parameters = [], callable $fn = null) {
        if (is_numeric($commandName) && count($parameters) < 1) {
            $parameters['value'] = $commandName;
            $commandName = 'HandleKeyInput';
        }
        if (is_string($parameters) || is_numeric($parameters)) {
            $parameters = array('value' => $parameters);
        } elseif (is_object($parameters)) {
            $parameters = (array)$parameters;
        }
        $parameters['name'] = $commandName;

        return $this->send('/udap/api/command', new Command(
            $parameters,
            'command'
        ), $fn);
    }

    public function queryData($targetId, $params=[], callable $fn) {
        $p='';

        foreach ($params as $key => $value) {
            $p.= '&'.$key.'='.$value;
        };

        return $this->send('/udap/api/data?target='.$targetId.$p, null, function($res) use ($fn) {
            if (is_object($res)) {
                if (isset($res->dataList)) {
                    $fn($res->dataList);
                } else {
                    $fn($res);
                }
            } else {
                $fn($res);
            }
        });
    }


    /**
     * APP 2 APP
     */

    public function appStatus($auid)
    {
        $var = $this->comm->sendRaw('/udap/api/apptoapp/data/'.$auid.'/status');

        return $var;
    }

    public function appAuid($name) {
        $var = $this->comm->sendRaw('/udap/api/apptoapp/data/'.$name);
        return $var;
    }

    /**
     * Ejecuta una app y el TV manda eventos de la misma a diferencia
     * de el comando que se ejecuta en /udap/api/command que no envia eventos.
     */
    public function appRun($auid) {
        $parameters['name'] = 'AppExecute';
        $parameters['auid'] = sprintf("%016x",$auid);
        $var = $this->comm->sendRaw('/udap/api/apptoapp/command/',new Command(
            $parameters,
            'command'
        ));
        return $var;
    }

    // public function netflix($contentID) {
    //     $parameters = [
    //         'name'                 => 'SearchCMDPlaySDPContent',
    //         "content_type"         => "1",
    //         "conts_exec_type"      => "20",
    //         "conts_plex_type_flag" => "N",
    //         "conts_search_id"      => "2023237",
    //         "conts_age"            => "18",
    //         "exec_id"              => "netflix",
    //         "item_id"              => "-Q m,ttp%3A%2F%2Fapi.netflix.com%2Fcatalog%2Ftitles%2Fmovies%2F" . $contentID . "&amp;source_type=4&amp;trackId=6054700&amp;trackUrl=https%3A%2F%2Fapi.netflix.com%2FAPI_APP_ID_6261%3F%23Search%3F",
    //         "app_type"             => ""
    //     ];
    //     $var = $this->comm->sendRaw('/udap/api/apptoapp/command/',new Command(
    //         $parameters,
    //         'command'
    //     ));
    //     return $var;
    // }
}
