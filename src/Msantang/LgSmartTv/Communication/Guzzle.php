<?php
namespace Msantang\LgSmartTv\Communication;

use Msantang\LgSmartTv\Interfaces\Communication;
use Msantang\LgSmartTv\Command;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Martin Alejandro Santangelo <msantangelo@smartsoftware.com.ar>
 */
class Guzzle implements Communication
{
    public $debug = true;

    protected $client;

    protected $base_url = '';

    public function __construct($ip, $port='8080', $opt = ['timeout'  => 2.0])
    {
        $this->client = new Client( $opt );

        $this->base_url = "http://$ip:$port";

    }

    public function sendRaw($url, $body = null, callable $fn = null)
    {
        //convertimos a texto el comando (__toString)
        $data = (string)$body;


        if ($data) $m='POST'; else $m='GET';

        $headers = [
            'Content-Type'   =>'text/xml; charset=utf-8',
            'User-Agent'     => 'UDAP/2.0',
            'Content-Length' => strlen($data)
        ];

        $request = new Request($m, $this->base_url.$url, $headers, $data);

        if($this->debug) echo "PETICION $m $url\n".$data;

        $that = $this;

        $promise = $this->client->sendAsync($request);

        $promise->then(
            function (ResponseInterface $res) use ($fn, $that) {

                if ($that->debug) {
                    echo 'RESPUESTA '.$res->getStatusCode()."\n";
                    print_r($res->getHeaders());
                    echo PHP_EOL.'---'.PHP_EOL;
                    echo $res->getBody();
                    echo PHP_EOL.'---'.PHP_EOL;
                }

                if (!$fn) return;

                $h = $res->getHeader('Content-Type');

                switch ($h) {
                    case 'image/jpeg':
                    case 'image/png':
                        $fn($res->getBody());
                        break;
                    case 'text/xml; charset=utf-8':
                        $fn($res->xml());
                        break;
                    default:
                        $fn($res->getBody());
                        break;
                }
            },
            function ($e) {
                $resp = $e->getResponse();

                if ($resp && $code == 401) {
                    throw new UnpairedException('Unauthorized');
                } else {
                    throw $e;
                }
            }
        );

        return $promise;
    }

    public function send($url, Command $data = null, callable $fn = null) {
        return $this->sendRaw($url, $data, $fn);
    }
}