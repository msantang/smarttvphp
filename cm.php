<?php
require 'vendor/autoload.php';

use Msantang\LgSmartTv\SmartTV;
use Msantang\LgSmartTv\Control;
use Msantang\LgSmartTv\Communication\Guzzle as Comm;

$key = '894188';
// $key = '864188';
$macAddressHexadecimal = '9C-80-DF-B6-D1-B8';

$tv = new SmartTV(new Comm('192.168.1.126'));

$control = new Control($tv);

$tv->debug = false;

$tv->setPairingKey($key);

switch ($argv[1]) {

    case 'hi':
        $tv->authenticate();
        break;

    case 'off':
        $control->cmdPower();
        break;
    case 'ok':
        $control->cmdOk();
        break;
    case 'exit':
        $control->cmdExit();
        break;
    case 'showkey':
        $tv->showKey();
         # code...
         break;

    case 'bye':
        $tv->bye();
         # code...
         break;

    case 'volup':
        $promise = $control->cmdVolumeUp();
        $promise->wait();
        break;
    case 'voldown':
        $promise = $control->cmdVolumeDown();
        $promise->wait();
        break;
    case 'channel':
        if (@$argv[2]) {
            $channels = $control->getChannelList();

            foreach ($channels->data as $c) {
                if ($c->physicalNum == $argv[2]) break;
            }

            $promise = $control->cmdChangeChannel($c);
            $promise->wait();
        } else {
            $promise = $control->getCurrentChannel([], function($c){print_r($c);});
            $promise->wait();
        }
        break;
    case 'list':
        $promise =  $control->getChannelList([], function($r) {
            print_r($r);
        });
        $promise->wait();
        break;
    case 'capture':

        $promise = $control->getScreen([], function($f) {file_put_contents('screen.jpeg', $f);});

        $promise->wait();
        break;

    case 'apps':
        $promise = $control->getApps(['type' => 1, 'index' => 1,'number'=>1],function($c){print_r($c);});
        $promise->wait();
        break;
    case 'mute':
        $promise = $control->cmdMuteToggle();
        $promise->wait();
        break;

    case 'run':
        if (!$argv[2]) {
            die('debe pasar el nombre de la aplicacion');
        }

        $apps = $control->getApps(['type' => 1, 'index' => 1,'number'=>1]);

        foreach ($apps->data as $c) {
            if (strtolower($c->name) == strtolower($argv[2])) {
                print_r($c);
                if (@$argv[3]) {
                    $control->cmdLaunchApp(['auid' => $c->auid, 'appname' => $c->name, 'contentId'=> $argv[3]]);
                } else {
                    $control->cmdLaunchApp(['auid' => $c->auid, 'appname' => $c->name]);
                }
                break;
            }
        }


    case 'apprun':
        $r = $tv->appRun($argv[2]);
        break;
    case 'status':
        $r = $tv->appStatus($argv[2]);
        echo "Estado: $r\n";
        break;
    case 'auid':
        $r = $tv->appAuid($argv[2]);
        echo "AUID: $r\n";
        break;
    case 'buscar':

        $tv->netflix($argv[2]);

        break;

    case 'test':
        echo $tv->processCommand('2ndTVStreaming',['device'=>'Android', 'value'=>0, 'duration'=>1, 'fileNum'=>'0']);
        break;
}

$queue = \GuzzleHttp\Promise\queue();
$queue->run();